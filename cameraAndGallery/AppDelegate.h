//
//  AppDelegate.h
//  cameraAndGallery
//
//  Created by Student-002 on 26/08/16.
//  Copyright © 2016 student 002. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

