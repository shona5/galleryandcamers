//
//  ViewController.h
//  cameraAndGallery
//
//  Created by Student-002 on 26/08/16.
//  Copyright © 2016 student 002. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgview;

- (IBAction)camera:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *gallery;
- (IBAction)gallery:(id)sender;

@end

