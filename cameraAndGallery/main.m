//
//  main.m
//  cameraAndGallery
//
//  Created by Student-002 on 26/08/16.
//  Copyright © 2016 student 002. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
